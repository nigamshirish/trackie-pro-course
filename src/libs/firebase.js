import firebase from 'firebase'

// Initialize Firebase
// Set the configuration for your app

const config = {
  apiKey: process.env.API_KEY,
  authDomain: process.env.AUTH_DOMAIN,
  databaseURL: process.env.DATABASE_URL,
  projectId: process.env.PROJECT_ID,
  storageBucket: process.env.STORAGE_BUCKET,
  messagingSenderId: process.env.MESSENGER_SENDER_ID
}

const fbApp = firebase.initializeApp(config)

export default fbApp
