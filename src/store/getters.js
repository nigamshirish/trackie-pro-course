// Importing underscore library
import _ from 'underscore'

export default {

  // Getter to retrieve all entries
  getEntries: (state) => state.entries,

  // Getter to retrieve logged in user
  currentUser: (state) => state.user,

  // Getter to check whether we have loggedin user or not
  hasCurrentUser: (state) => !_.isEmpty(state.user)

}
