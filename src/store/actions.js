// Importing router for navigation access
import router from '../router'
// Importing auth feature from firebase library
import { auth } from 'firebase'
// Importing initialized firebase app
import fbApp from '../libs/firebase'
// Importing mutation constants
import * as types from './mutation-types'
// Importing global event bus to trigger and receive events
import { Bus } from '../libs/bus'
// Accessing firebase database instance
const db = fbApp.database()

// Notification helpers for success and error notifications
const notifyError = (message) => Bus.$emit('show-notification', { type: 'error', message: message })
const notifySuccess = (message) => Bus.$emit('show-notification', { type: 'success', message: message })

export default {

  // Action to fetch all entries of logged in user
  fetchEntries: ({commit, state}) => {
    db.ref(`entries/${state.user.uid}`).once('value')
      .then(snapshot => {
        // Loading up any available entries in local state
        commit(types.SET_ENTRIES, snapshot.val())
      },
      () => notifyError('Failed to retrieve tasks'))
  },

  // Action to add entry
  addEntry: ({commit, state}, payload) => {
    const ref = db.ref(`entries/${state.user.uid}`).push(payload)
    ref.then(val => {
      commit(types.ADD_ENTRY, { data: payload, key: ref.key })
      notifySuccess('Your entry has been saved')
    },
    () => notifyError('Failed to add entry'))
  },

  // Action to update entry
  updateEntry: ({commit, state}, payload) => {
    const ref = db.ref(`entries/${state.user.uid}/${payload.key}`).set(payload.data)
    ref.then(val => {
      commit(types.UPDATE_ENTRY, { data: payload.data, key: payload.key })
      notifySuccess('Your entry has been saved')
    },
    () => notifyError('Failed to update entry'))
  },

  // Action to delete entry
  deleteEntry: ({commit, state}, payload) => {
    const toDelete = db.ref(`entries/${state.user.uid}/${payload.key}`).remove()
    toDelete.then(() => {
      commit(types.DELETE_ENTRY, { key: payload.key })
      notifySuccess('Your entry has been deleted')
    },
    () => notifyError('Failed to delete entry'))
  },

  // Action to set user
  setUser: ({commit, state}, user) => {
    commit(types.SET_USER, user)
  },

  // Action to unset user
  unSetUser: ({commit, state}) => {
    commit(types.UNSET_USER)
  },

  // Action to signin the user using google authentication
  signInWithGoogle: ({ dispatch, commit }) => {
    fbApp.auth().signInWithPopup(new auth.GoogleAuthProvider())
      .then((result) => {
        dispatch('setUser', result.user)
        dispatch('fetchEntries')
        router.replace('home')
      })
  },

  // Action to signout user
  signOut: ({ commit }) => {
    fbApp.auth().signOut()
      .then(() => {
        commit(types.UNSET_USER)
        router.replace('login')
      }, () => notifyError('Failed to sign out'))
  }

}
