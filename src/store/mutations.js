// Importing Vue instance
import Vue from 'vue'
// Importing mutation types
import * as types from './mutation-types'

export default {
  // Mutation to set entries in state
  [types.SET_ENTRIES] (state, entries) {
    state.entries = entries || {}
  },

  // Mutation to add entry in state
  [types.ADD_ENTRY] (state, payload) {
    Vue.set(state.entries, payload.key, payload.data)
  },

  // Mutation to add entry in state
  [types.UPDATE_ENTRY] (state, payload) {
    Vue.set(state.entries, payload.key, payload.data)
  },

  // Mutation to add entry in state
  [types.DELETE_ENTRY] (state, payload) {
    Vue.delete(state.entries, payload.key)
  },

  // Mutation to set user
  [types.SET_USER] (state, result) {
    state.user = result
  },

  // Mutation to unset user
  [types.UNSET_USER] (state) {
    state.user = {}
    state.entries = {}
  }
}
