// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
// Importing application layout component
import App from './App'
// Importing an initialised vue-router with application routes
import router from './router'
// Importing an initialised vuex store with state, getters, actions and mutations wired up
import store from './store'
// Importing an initialised firebase app
import fbApp from './libs/firebase'

// Importing vuetify components
import {
  Vuetify,
  VApp,
  VNavigationDrawer,
  VFooter,
  VCard,
  VSnackbar,
  VTextField,
  VSwitch,
  VSlider,
  VDatePicker,
  VTimePicker,
  VDivider,
  VDialog,
  VList,
  VBtn,
  VIcon,
  VGrid,
  VToolbar,
  transitions
} from 'vuetify'

// Importing vuetify theme
import '../node_modules/vuetify/src/stylus/app.styl'

// Registering vuetify and imported vuetify components with Vue
Vue.use(Vuetify, {
  components: {
    VApp,
    VNavigationDrawer,
    VFooter,
    VCard,
    VSnackbar,
    VTextField,
    VSwitch,
    VSlider,
    VDatePicker,
    VTimePicker,
    VDivider,
    VDialog,
    VList,
    VBtn,
    VIcon,
    VGrid,
    VToolbar,
    transitions
  }
})

Vue.config.productionTip = false

// Listening to onAuthStateChanged event of firebase to
// initialise Vue with application layout, store and router
const unsubscribe = fbApp.auth().onAuthStateChanged((user) => {
  // Dispatching vuex setUser action to update state with logged in user
  store.dispatch('setUser', user)
  /* eslint-disable no-new */
  new Vue({
    el: '#app',
    router,
    store,
    components: { App },
    template: '<App/>'
  })
  unsubscribe()
})
