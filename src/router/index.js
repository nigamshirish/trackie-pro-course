import Vue from 'vue'
import Router from 'vue-router'
// Importing Home component
import Home from '@/components/Home'
// Importing FoodEntry component
import FoodEntry from '@/components/FoodEntry'
// Importing WorkoutEntry component
import WorkoutEntry from '@/components/WorkoutEntry'
// Importing MedicineEntry component
import MedicineEntry from '@/components/MedicineEntry'
// Importing ReadingEntry component
import ReadingEntry from '@/components/ReadingEntry'
// Importing login component
import Login from '@/components/shared/Login'
// Importing our store to get access to getters
import store from '../store'

Vue.use(Router)

let router = new Router({
  // Enabling history mode and clean urls without #! (hash bang)
  mode: 'history',
  // Defining our application routes
  routes: [
    // Catch all route to redirect user to login page
    {
      path: '*',
      redirect: '/login'
    },
    // Securing all application routes even the / route, redirect to login page
    {
      path: '/',
      redirect: 'login'
    },
    // Login route. This route does not require authentication
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    // The following are protected routes. Only authenticated user can access it.
    // using meta.requiresAuth = true to denote authentication requirements
    // Home route
    {
      path: '/home',
      name: 'Home',
      component: Home,
      meta: {
        requiresAuth: true
      }
    },
    // Food entry route
    {
      path: '/entries/add/food-entry',
      name: 'FoodEntry',
      component: FoodEntry,
      meta: {
        requiresAuth: true
      }
    },
    // Food entry route
    {
      path: '/entries/add/workout-entry',
      name: 'WorkoutEntry',
      component: WorkoutEntry,
      meta: {
        requiresAuth: true
      }
    },
    // Medicine entry route
    {
      path: '/entries/add/medicine-entry',
      name: 'MedicineEntry',
      component: MedicineEntry,
      meta: {
        requiresAuth: true
      }
    },
    // Reading entry route
    {
      path: '/entries/add/reading-entry',
      name: 'ReadingEntry',
      component: ReadingEntry,
      meta: {
        requiresAuth: true
      }
    }

  ]
})

// Navigation guard (beforeEach) will execute before each navigation.
// If the route requires Authentication and user is not logged in, then send the user to login route.
// If the route does not require and user is logged in, then send the user to home route.
// Otherwise let user to navigate the app.
router.beforeEach((to, from, next) => {
  // Check if the user information is available
  let hasCurrentUser = store.getters.hasCurrentUser
  // Check if target route require authentication
  let requiresAuth = to.matched.some(record => record.meta.requiresAuth)
  // if route requires authentication and user is not logged in then redirect to login route
  if (requiresAuth && !hasCurrentUser) next('login')
  // This will catch 404 not found routes as well
  else if (!requiresAuth && hasCurrentUser) next('home')
  // otherwise let the user access the route
  else next()
})

// Exporting the router instance
export default router
