'use strict'

const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

const envConf = require('dotenv').config()

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  API_KEY: JSON.stringify(process.env.API_KEY || envConf.API_KEY) ,
  AUTH_DOMAIN: JSON.stringify(process.env.AUTH_DOMAIN || envCONF.AUTH_DOMAIN) ,
  DATABASE_URL: JSON.stringify(process.env.DATABASE_URL || envConf.DATABASE_URL) ,
  PROJECT_ID: JSON.stringify(process.env.PROJECT_ID || envConf.PROJECT_ID) ,
  STORAGE_BUCKET: JSON.stringify(process.env.STORAGE_BUCKET || envConf.STORAGE_BUCKET) ,
  MESSENGER_SENDER_ID: JSON.stringify(process.env.MESSENGER_SENDER_ID || envConf.MESSENGER_SENDER_ID)
})